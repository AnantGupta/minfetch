# Minfetch

A minimal fetch program, no fancy stuff with clean code written in bash.

First of all, I am learning shell scripting. I switched to Linux 7-8 months ago so that's why the code might be a bit messy for people, also it is not fancy that's why :)

## Screenshots

![Minfetch screenshot](https://i.imgur.com/Ru9jFOw.png)

## Installation

Install minfetch by, first of all, note that you should have `git`

```bash
git clone https://gitlab.com/AnantGupta/minfetch.git
cd minfetch
sudo cp minfetch /bin
```
    
## Run!

Just run by

``` bash
minfetch
```
## 🚀 About Me
I'm Anant Gupta, founder of minfetch (not very proud but still this is true)... I have other projects like a [Non-Profit website](https://gitlab.com/AnantGupta/non-profit), [Terminal Help](https://gitlab.com/AnantGupta/terminal-help), [A chatbot for ATL Innovation Mission](https://gitlab.com/AnantGupta/chatbot-web), that's it lol.
